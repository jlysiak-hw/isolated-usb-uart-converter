EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text GLabel 9850 1375 2    50   UnSpc ~ 0
V_2
Wire Wire Line
	9850 1375 9300 1375
$Comp
L Device:R R1
U 1 1 61724FE7
P 9450 1475
F 0 "R1" V 9500 1625 50  0000 C CNN
F 1 "100R" V 9500 1275 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric_Pad0.72x0.64mm_HandSolder" V 9380 1475 50  0001 C CNN
F 3 "~" H 9450 1475 50  0001 C CNN
	1    9450 1475
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R2
U 1 1 6172D07F
P 9450 1675
F 0 "R2" V 9400 1525 50  0000 C CNN
F 1 "100R" V 9400 1875 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric_Pad0.72x0.64mm_HandSolder" V 9380 1675 50  0001 C CNN
F 3 "~" H 9450 1675 50  0001 C CNN
	1    9450 1675
	0    1    1    0   
$EndComp
Wire Wire Line
	9150 1675 9300 1675
Wire Wire Line
	9150 1475 9300 1475
Wire Wire Line
	9600 1475 9850 1475
Wire Wire Line
	9600 1675 9850 1675
Text GLabel 9850 1475 2    50   Input ~ 0
TX_2
Text GLabel 9850 1675 2    50   Output ~ 0
RX_2
Text Notes 3650 775  0    50   ~ 0
IZOLATED UART
$Comp
L Interface_USB:MCP2200-I-SO U4
U 1 1 61757984
P 4375 3375
F 0 "U4" H 4250 2625 50  0000 C CNN
F 1 "MCP2200-I-SO" H 3875 2625 50  0000 C CNN
F 2 "Package_SO:SOIC-20W_7.5x12.8mm_P1.27mm" H 4375 2225 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/200022228D.pdf" H 4375 2375 50  0001 C CNN
	1    4375 3375
	-1   0    0    -1  
$EndComp
$Comp
L Connector:USB_B J1
U 1 1 6176607D
P 925 2050
F 0 "J1" H 982 2517 50  0000 C CNN
F 1 "USB_B" H 982 2426 50  0000 C CNN
F 2 "Connector_USB:USB_B_Lumberg_2411_02_Horizontal" H 1075 2000 50  0001 C CNN
F 3 " ~" H 1075 2000 50  0001 C CNN
	1    925  2050
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 61789671
P 3950 2400
F 0 "C2" V 4025 2475 50  0000 L CNN
F 1 "min 220nF/10V" V 4100 1975 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 3988 2250 50  0001 C CNN
F 3 "~" H 3950 2400 50  0001 C CNN
	1    3950 2400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4275 2400 4275 2575
Wire Wire Line
	4475 1975 4475 2575
$Comp
L Device:C C1
U 1 1 617ADDD3
P 4175 1975
F 0 "C1" V 4100 1800 50  0000 L CNN
F 1 "min 220nF/10V" V 4025 1575 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 4213 1825 50  0001 C CNN
F 3 "~" H 4175 1975 50  0001 C CNN
	1    4175 1975
	0    1    1    0   
$EndComp
$Comp
L Device:C C3
U 1 1 617D19A4
P 2675 4950
F 0 "C3" H 2525 5050 50  0000 C CNN
F 1 "27pF/10V" H 2475 4850 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 2713 4800 50  0001 C CNN
F 3 "~" H 2675 4950 50  0001 C CNN
	1    2675 4950
	1    0    0    -1  
$EndComp
$Comp
L Device:C C4
U 1 1 617D22F4
P 3225 4975
F 0 "C4" H 3325 5075 50  0000 C CNN
F 1 "27pF/10V" H 3425 4900 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 3263 4825 50  0001 C CNN
F 3 "~" H 3225 4975 50  0001 C CNN
	1    3225 4975
	1    0    0    -1  
$EndComp
NoConn ~ 825  2450
Wire Wire Line
	8075 1375 8125 1375
Wire Wire Line
	5425 2875 5275 2875
Wire Wire Line
	8150 1775 8100 1775
Text Notes 3050 900  0    50   ~ 0
FULL IZOLATION OF CABLE CONNECTED DEVICE
$Comp
L Device:R R4
U 1 1 61861E5F
P 5275 2500
F 0 "R4" H 5205 2454 50  0000 R CNN
F 1 "10k" H 5205 2545 50  0000 R CNN
F 2 "Resistor_SMD:R_0402_1005Metric_Pad0.72x0.64mm_HandSolder" V 5205 2500 50  0001 C CNN
F 3 "~" H 5275 2500 50  0001 C CNN
	1    5275 2500
	-1   0    0    1   
$EndComp
Wire Wire Line
	5275 2775 5275 2650
Wire Wire Line
	5275 2350 5275 1975
$Comp
L Device:R R6
U 1 1 618C2723
P 10400 2550
F 0 "R6" H 10470 2596 50  0000 L CNN
F 1 "10k/0.125W" H 10470 2505 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric_Pad0.72x0.64mm_HandSolder" V 10330 2550 50  0001 C CNN
F 3 "~" H 10400 2550 50  0001 C CNN
	1    10400 2550
	1    0    0    -1  
$EndComp
NoConn ~ 5275 3475
NoConn ~ 5275 3575
NoConn ~ 5275 3675
NoConn ~ 5275 3775
Text GLabel 1275 1850 2    50   UnSpc ~ 0
VBUS
Wire Wire Line
	1225 1850 1275 1850
Text GLabel 1275 2525 2    50   UnSpc ~ 0
GNDBUS
Wire Wire Line
	1275 2525 925  2525
Wire Wire Line
	925  2525 925  2450
Wire Wire Line
	1225 2150 1275 2150
Wire Wire Line
	1225 2050 1275 2050
Text GLabel 1275 2050 2    50   Output ~ 0
USB_D_+
Text GLabel 1275 2150 2    50   Output ~ 0
USB_D_-
Wire Wire Line
	8150 1475 8075 1475
Text GLabel 8075 1375 0    50   UnSpc ~ 0
VBUS
Text GLabel 8075 1475 0    50   Output ~ 0
RX_1
$Comp
L Isolator:ADuM1201AR U1
U 1 1 617153B4
P 8650 1575
F 0 "U1" H 8650 2042 50  0000 C CNN
F 1 "ADuM1201AR" H 8650 1951 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 8650 1175 50  0001 C CIN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/ADuM1200_1201.pdf" H 8650 1475 50  0001 C CNN
	1    8650 1575
	1    0    0    -1  
$EndComp
Wire Wire Line
	8075 1675 8150 1675
Text GLabel 8075 1675 0    50   Input ~ 0
TX_1
Text GLabel 8100 1775 0    50   UnSpc ~ 0
GNDBUS
Text GLabel 9850 1775 2    50   UnSpc ~ 0
GND_2
Wire Wire Line
	9150 1775 9850 1775
Text GLabel 9850 2200 2    50   UnSpc ~ 0
V_2
Wire Wire Line
	9850 2200 9300 2200
$Comp
L Device:R R3
U 1 1 611E3D19
P 10725 2300
F 0 "R3" V 10775 2450 50  0000 C CNN
F 1 "100R" V 10775 2100 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric_Pad0.72x0.64mm_HandSolder" V 10655 2300 50  0001 C CNN
F 3 "~" H 10725 2300 50  0001 C CNN
	1    10725 2300
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R5
U 1 1 611E3D1F
P 9450 2500
F 0 "R5" V 9400 2350 50  0000 C CNN
F 1 "100R" V 9400 2700 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric_Pad0.72x0.64mm_HandSolder" V 9380 2500 50  0001 C CNN
F 3 "~" H 9450 2500 50  0001 C CNN
	1    9450 2500
	0    1    1    0   
$EndComp
Wire Wire Line
	9150 2500 9300 2500
Wire Wire Line
	9150 2300 10400 2300
Wire Wire Line
	10875 2300 11000 2300
Wire Wire Line
	9600 2500 9850 2500
Text GLabel 11000 2300 2    50   Input ~ 0
RTS_2
Text GLabel 9850 2500 2    50   Output ~ 0
CTS_2
Wire Wire Line
	8075 2200 8100 2200
Wire Wire Line
	8150 2600 8100 2600
Wire Wire Line
	8150 2300 8075 2300
Text GLabel 8075 2200 0    50   UnSpc ~ 0
VBUS
Text GLabel 8075 2300 0    50   Output ~ 0
CTS_1
$Comp
L Isolator:ADuM1201AR U2
U 1 1 611E3D30
P 8650 2400
F 0 "U2" H 8650 2867 50  0000 C CNN
F 1 "ADuM1201AR" H 8650 2776 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 8650 2000 50  0001 C CIN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/ADuM1200_1201.pdf" H 8650 2300 50  0001 C CNN
	1    8650 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	8075 2500 8150 2500
Text GLabel 8075 2500 0    50   Input ~ 0
RTS_1
Text GLabel 8100 2600 0    50   UnSpc ~ 0
GNDBUS
Text GLabel 9850 2600 2    50   UnSpc ~ 0
GND_2
Wire Wire Line
	9150 2600 9850 2600
Text GLabel 3750 1975 0    50   UnSpc ~ 0
GNDBUS
Wire Wire Line
	3800 2400 3750 2400
Text GLabel 3750 2400 0    50   UnSpc ~ 0
GNDBUS
Wire Wire Line
	4325 1975 4475 1975
Wire Wire Line
	4100 2400 4275 2400
Wire Wire Line
	3750 1975 4025 1975
Wire Wire Line
	4475 1975 5275 1975
Connection ~ 4475 1975
Text GLabel 4475 1650 1    50   UnSpc ~ 0
VBUS
Wire Wire Line
	4475 1650 4475 1975
Wire Wire Line
	5275 2975 5425 2975
Text GLabel 5425 2875 2    50   Input ~ 0
RX_1
Wire Wire Line
	6300 4050 6225 4050
Wire Wire Line
	6300 3875 6300 4050
Wire Wire Line
	6225 3875 6300 3875
Wire Wire Line
	5925 4050 5850 4050
$Comp
L Device:R R12
U 1 1 618A47CE
P 6075 4050
F 0 "R12" V 6150 3950 50  0000 L CNN
F 1 "1k/0.125W" V 6225 3950 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric_Pad0.72x0.64mm_HandSolder" V 6005 4050 50  0001 C CNN
F 3 "~" H 6075 4050 50  0001 C CNN
	1    6075 4050
	0    1    1    0   
$EndComp
$Comp
L Device:LED D2
U 1 1 61888554
P 5700 4050
F 0 "D2" H 5775 3925 50  0000 R CNN
F 1 "TXLED" H 5775 3850 50  0000 R CNN
F 2 "LED_SMD:LED_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 5700 4050 50  0001 C CNN
F 3 "~" H 5700 4050 50  0001 C CNN
	1    5700 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5850 3875 5925 3875
$Comp
L Device:LED D1
U 1 1 618763D8
P 5700 3875
F 0 "D1" H 5775 3975 50  0000 R CNN
F 1 "RXLED" H 5775 4050 50  0000 R CNN
F 2 "LED_SMD:LED_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 5700 3875 50  0001 C CNN
F 3 "~" H 5700 3875 50  0001 C CNN
	1    5700 3875
	1    0    0    -1  
$EndComp
$Comp
L Device:R R11
U 1 1 618763D2
P 6075 3875
F 0 "R11" V 5925 3775 50  0000 L CNN
F 1 "1k/0.125W" V 6000 3775 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric_Pad0.72x0.64mm_HandSolder" V 6005 3875 50  0001 C CNN
F 3 "~" H 6075 3875 50  0001 C CNN
	1    6075 3875
	0    1    1    0   
$EndComp
Text GLabel 5425 2975 2    50   Output ~ 0
TX_1
Wire Wire Line
	5275 3075 5425 3075
Wire Wire Line
	5275 3175 6225 3175
Text GLabel 5425 3075 2    50   Output ~ 0
RTS_1
Text GLabel 6975 3175 2    50   Input ~ 0
CTS_1
$Comp
L Isolator:ADuM120N U3
U 1 1 61215EB3
P 8650 3275
F 0 "U3" H 8650 3742 50  0000 C CNN
F 1 "ADuM120N" H 8650 3651 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 8650 2875 50  0001 C CIN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/ADuM120N_121N.pdf" H 8200 3675 50  0001 C CNN
	1    8650 3275
	1    0    0    -1  
$EndComp
Wire Wire Line
	5275 3275 5425 3275
Wire Wire Line
	5425 3375 5275 3375
Wire Wire Line
	5550 3875 5275 3875
Wire Wire Line
	5325 4050 5325 3975
Wire Wire Line
	5325 3975 5275 3975
Wire Wire Line
	5325 4050 5550 4050
Wire Wire Line
	6300 3875 6400 3875
Connection ~ 6300 3875
Text GLabel 6400 3875 2    50   UnSpc ~ 0
VBUS
Wire Wire Line
	4375 4175 4375 4325
Text GLabel 4375 4325 3    50   UnSpc ~ 0
GNDBUS
$Comp
L Device:Crystal_GND24_Small Y1
U 1 1 6123109B
P 2925 4650
F 0 "Y1" H 2800 4500 50  0000 L CNN
F 1 "12MHz,18pF" H 2700 4850 50  0000 L CNN
F 2 "Crystal:Crystal_SMD_5032-4Pin_5.0x3.2mm" H 2925 4650 50  0001 C CNN
F 3 "https://www.tme.eu/Document/ba6ed70d9526495da83bd01b59cec2b4/LFXTAL032878Reel.pdf" H 2925 4650 50  0001 C CNN
F 4 "https://www.tme.eu/ie/en/details/12.00m-cfpx104/smd-quartz-crystals/iqd-frequency-products/lfxtal032878reel/" H 2925 4650 50  0001 C CNN "Url"
	1    2925 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	2925 4550 2925 4525
Wire Wire Line
	3075 4525 3075 4800
Wire Wire Line
	3075 4800 2925 4800
Wire Wire Line
	2925 4800 2925 4750
Wire Wire Line
	2925 4525 3075 4525
Wire Wire Line
	2675 4800 2675 4650
Wire Wire Line
	2675 4650 2825 4650
Wire Wire Line
	3025 4650 3225 4650
Wire Wire Line
	3225 4650 3225 4825
Wire Wire Line
	2675 5100 2675 5175
Wire Wire Line
	2675 5175 2925 5175
Wire Wire Line
	3225 5175 3225 5125
Wire Wire Line
	2925 4800 2925 5175
Connection ~ 2925 4800
Connection ~ 2925 5175
Wire Wire Line
	2925 5175 3225 5175
Wire Wire Line
	2925 5175 2925 5250
Text GLabel 2925 5250 3    50   UnSpc ~ 0
GNDBUS
Wire Wire Line
	3225 4650 3225 4400
Connection ~ 3225 4650
Connection ~ 2675 4650
$Comp
L Device:R R13
U 1 1 612453DC
P 3225 4250
F 0 "R13" H 3000 4300 50  0000 L CNN
F 1 "50/0.125W" H 2750 4400 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric_Pad0.72x0.64mm_HandSolder" V 3155 4250 50  0001 C CNN
F 3 "~" H 3225 4250 50  0001 C CNN
	1    3225 4250
	-1   0    0    1   
$EndComp
Wire Wire Line
	3225 4100 3225 3975
Wire Wire Line
	3225 3975 3475 3975
Wire Wire Line
	3475 3775 2675 3775
Wire Wire Line
	2675 3775 2675 4650
Text Notes 3250 4600 0    50   ~ 0
may required if \nquartz drive level is high
Text Notes 1900 4925 0    50   ~ 0
to check
Text Notes 12475 1300 0    50   ~ 0
The SSPND pin (if enabled) reflects the USB state\n(Suspend/Resume). The pin is active ‘low’ when the\nSuspend state has been issued by the USB host.\nLikewise, the pin drives ‘high’ after the Resume state is\nachieved.\nThis pin allows the application to go into Low Power\nmode when USB communication is suspended, and\nswitches to a full active state when USB activity is\nresumed.
Text Notes 12475 1850 0    50   ~ 0
The USBCFG pin (if enabled) starts out ‘low’ during\npower-up or after Reset, and goes ‘high’ after the\ndevice successfully configures to the USB. The pin will\ngo ‘low’ when in Suspend mode and ‘high’ when the\nUSB resumes.
Wire Wire Line
	10400 2400 10400 2300
Connection ~ 10400 2300
Wire Wire Line
	10400 2300 10575 2300
Wire Wire Line
	10400 2700 10400 2750
Text GLabel 10400 2750 3    50   UnSpc ~ 0
GND_2
$Comp
L Device:R R10
U 1 1 6125B1CA
P 6225 3450
F 0 "R10" H 6295 3496 50  0000 L CNN
F 1 "10k/0.125W" H 6295 3405 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric_Pad0.72x0.64mm_HandSolder" V 6155 3450 50  0001 C CNN
F 3 "~" H 6225 3450 50  0001 C CNN
	1    6225 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	6225 3300 6225 3175
$Comp
L Device:R R7
U 1 1 612650F0
P 6475 3175
F 0 "R7" V 6375 3150 50  0000 L CNN
F 1 "0" H 6545 3130 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric_Pad0.72x0.64mm_HandSolder" V 6405 3175 50  0001 C CNN
F 3 "~" H 6475 3175 50  0001 C CNN
	1    6475 3175
	0    1    1    0   
$EndComp
Wire Wire Line
	6325 3175 6225 3175
Connection ~ 6225 3175
Wire Wire Line
	6975 3175 6625 3175
Wire Wire Line
	6225 3600 6225 3650
Wire Wire Line
	6225 3650 6300 3650
Text GLabel 6300 3650 2    50   UnSpc ~ 0
GNDBUS
Text Notes 6475 3425 0    50   ~ 0
optional\nif ADUM not mounted
Text GLabel 5425 3375 2    50   Output ~ 0
USBCFG_1
Text GLabel 5425 3275 2    50   Output ~ 0
SUSPEND_1
Text GLabel 8125 3375 0    50   Input ~ 0
USBCFG_1
Text GLabel 8125 3175 0    50   Input ~ 0
SUSPEND_1
Wire Wire Line
	8125 3175 8150 3175
Wire Wire Line
	8125 3375 8150 3375
Wire Wire Line
	8150 3075 8100 3075
Wire Wire Line
	8150 3475 8075 3475
Text GLabel 8075 3075 0    50   UnSpc ~ 0
VBUS
Text GLabel 8075 3475 0    50   UnSpc ~ 0
GNDBUS
Text GLabel 3300 2775 0    50   Input ~ 0
D_+
Text GLabel 3300 2975 0    50   Input ~ 0
D_-
Wire Wire Line
	3300 2775 3475 2775
Wire Wire Line
	3300 2975 3475 2975
Text GLabel 9325 3075 2    50   UnSpc ~ 0
V_2
Wire Wire Line
	9150 3075 9200 3075
Text GLabel 9325 3475 2    50   UnSpc ~ 0
GND_2
Wire Wire Line
	9150 3475 9325 3475
Text GLabel 9825 3175 2    50   Output ~ 0
SUSPEND_2
Text GLabel 9825 3375 2    50   Output ~ 0
USBCFG_2
Wire Wire Line
	9150 3175 9325 3175
Wire Wire Line
	9325 3375 9150 3375
$Comp
L Device:R R8
U 1 1 6129B7CD
P 9475 3175
F 0 "R8" V 9525 3025 50  0000 C CNN
F 1 "100R" V 9425 3375 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric_Pad0.72x0.64mm_HandSolder" V 9405 3175 50  0001 C CNN
F 3 "~" H 9475 3175 50  0001 C CNN
	1    9475 3175
	0    1    1    0   
$EndComp
$Comp
L Device:R R9
U 1 1 6129BCCF
P 9475 3375
F 0 "R9" V 9425 3225 50  0000 C CNN
F 1 "100R" V 9425 3575 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric_Pad0.72x0.64mm_HandSolder" V 9405 3375 50  0001 C CNN
F 3 "~" H 9475 3375 50  0001 C CNN
	1    9475 3375
	0    1    1    0   
$EndComp
Wire Wire Line
	9625 3375 9825 3375
Wire Wire Line
	9625 3175 9825 3175
$Comp
L Connector_Generic:Conn_02x07_Odd_Even J2
U 1 1 612B4578
P 12625 2875
F 0 "J2" H 12675 3392 50  0000 C CNN
F 1 "1.27 pitch 02x07 conn " H 12675 3301 50  0000 C CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_2x07_P1.27mm_Vertical" H 12625 2875 50  0001 C CNN
F 3 "~" H 12625 2875 50  0001 C CNN
	1    12625 2875
	1    0    0    -1  
$EndComp
Text GLabel 12300 2575 0    50   UnSpc ~ 0
V_2
Text GLabel 12300 2675 0    50   Output ~ 0
TX_2
Text GLabel 12300 2775 0    50   Input ~ 0
RX_2
Text GLabel 13075 2575 2    50   UnSpc ~ 0
GND_2
Wire Wire Line
	13075 2575 13000 2575
Wire Wire Line
	13000 2575 13000 2675
Wire Wire Line
	13000 3175 12925 3175
Connection ~ 13000 2575
Wire Wire Line
	13000 2575 12925 2575
Wire Wire Line
	12925 3075 13000 3075
Connection ~ 13000 3075
Wire Wire Line
	13000 3075 13000 3175
Wire Wire Line
	12925 2975 13000 2975
Connection ~ 13000 2975
Wire Wire Line
	13000 2975 13000 3075
Wire Wire Line
	12925 2875 13000 2875
Connection ~ 13000 2875
Wire Wire Line
	13000 2875 13000 2975
Wire Wire Line
	12925 2775 13000 2775
Connection ~ 13000 2775
Wire Wire Line
	13000 2775 13000 2875
Wire Wire Line
	12925 2675 13000 2675
Connection ~ 13000 2675
Wire Wire Line
	13000 2675 13000 2775
Wire Wire Line
	12300 2575 12425 2575
Wire Wire Line
	12300 2675 12425 2675
Wire Wire Line
	12300 2775 12425 2775
Text GLabel 12300 2875 0    50   Output ~ 0
RTS_2
Wire Wire Line
	12300 2875 12425 2875
Text GLabel 12300 2975 0    50   Input ~ 0
CTS_2
Wire Wire Line
	12425 2975 12300 2975
Text GLabel 12300 3075 0    50   Input ~ 0
SUSPEND_2
Text GLabel 12300 3175 0    50   Input ~ 0
USBCFG_2
Wire Wire Line
	12300 3075 12425 3075
Wire Wire Line
	12300 3175 12425 3175
$Comp
L Device:C C6
U 1 1 6134FE07
P 9550 1225
F 0 "C6" V 9475 1050 50  0000 L CNN
F 1 "100nF" V 9400 825 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 9588 1075 50  0001 C CNN
F 3 "~" H 9550 1225 50  0001 C CNN
	1    9550 1225
	0    1    1    0   
$EndComp
Wire Wire Line
	9400 1225 9300 1225
Wire Wire Line
	9300 1225 9300 1375
Connection ~ 9300 1375
Wire Wire Line
	9300 1375 9150 1375
Text GLabel 9850 1225 2    50   UnSpc ~ 0
GND_2
Wire Wire Line
	9850 1225 9700 1225
$Comp
L Device:C C8
U 1 1 6135907C
P 9550 2050
F 0 "C8" V 9475 1875 50  0000 L CNN
F 1 "100nF" V 9400 1650 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 9588 1900 50  0001 C CNN
F 3 "~" H 9550 2050 50  0001 C CNN
	1    9550 2050
	0    1    1    0   
$EndComp
Text GLabel 9850 2050 2    50   UnSpc ~ 0
GND_2
Wire Wire Line
	9850 2050 9700 2050
$Comp
L Device:C C9
U 1 1 6135D8E7
P 9400 2900
F 0 "C9" V 9325 2725 50  0000 L CNN
F 1 "100nF" V 9250 2500 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 9438 2750 50  0001 C CNN
F 3 "~" H 9400 2900 50  0001 C CNN
	1    9400 2900
	0    1    1    0   
$EndComp
Text GLabel 9700 2900 2    50   UnSpc ~ 0
GND_2
Wire Wire Line
	9700 2900 9550 2900
Wire Wire Line
	9400 2050 9300 2050
Wire Wire Line
	9300 2050 9300 2200
Connection ~ 9300 2200
Wire Wire Line
	9300 2200 9150 2200
Wire Wire Line
	9250 2900 9200 2900
Wire Wire Line
	9200 2900 9200 3075
Connection ~ 9200 3075
Wire Wire Line
	9200 3075 9325 3075
$Comp
L Device:C C5
U 1 1 6136AFC3
P 7750 1175
F 0 "C5" V 7675 1000 50  0000 L CNN
F 1 "100nF" V 7600 775 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 7788 1025 50  0001 C CNN
F 3 "~" H 7750 1175 50  0001 C CNN
	1    7750 1175
	0    1    1    0   
$EndComp
$Comp
L Device:C C7
U 1 1 6136B30B
P 7750 2025
F 0 "C7" V 7675 1850 50  0000 L CNN
F 1 "100nF" V 7600 1625 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 7788 1875 50  0001 C CNN
F 3 "~" H 7750 2025 50  0001 C CNN
	1    7750 2025
	0    1    1    0   
$EndComp
$Comp
L Device:C C10
U 1 1 6136B690
P 7750 2925
F 0 "C10" V 7675 2750 50  0000 L CNN
F 1 "100nF" V 7600 2525 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 7788 2775 50  0001 C CNN
F 3 "~" H 7750 2925 50  0001 C CNN
	1    7750 2925
	0    1    1    0   
$EndComp
Wire Wire Line
	7900 2925 8100 2925
Wire Wire Line
	8100 2925 8100 3075
Connection ~ 8100 3075
Wire Wire Line
	8100 3075 8075 3075
Wire Wire Line
	7900 2025 8100 2025
Wire Wire Line
	8100 2025 8100 2200
Connection ~ 8100 2200
Wire Wire Line
	8100 2200 8150 2200
Wire Wire Line
	7900 1175 8125 1175
Wire Wire Line
	8125 1175 8125 1375
Connection ~ 8125 1375
Wire Wire Line
	8125 1375 8150 1375
Text GLabel 7550 1175 0    50   UnSpc ~ 0
GNDBUS
Wire Wire Line
	7550 1175 7600 1175
Text GLabel 7525 2025 0    50   UnSpc ~ 0
GNDBUS
Wire Wire Line
	7525 2025 7600 2025
Text GLabel 7550 2925 0    50   UnSpc ~ 0
GNDBUS
Wire Wire Line
	7550 2925 7600 2925
$Comp
L Power_Protection:USBLC6-2SC6 U5
U 1 1 613D11FF
P 2475 1425
F 0 "U5" H 2600 1850 50  0000 C CNN
F 1 "USBLC6-2SC6" H 2825 1775 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-6" H 2475 925 50  0001 C CNN
F 3 "https://www.st.com/resource/en/datasheet/usblc6-2.pdf" H 2675 1775 50  0001 C CNN
	1    2475 1425
	1    0    0    -1  
$EndComp
Text GLabel 2475 1875 3    50   UnSpc ~ 0
GNDBUS
Wire Wire Line
	2475 1875 2475 1825
Text GLabel 2475 925  1    50   UnSpc ~ 0
VBUS
Wire Wire Line
	2475 925  2475 1025
Text GLabel 2975 1325 2    50   Input ~ 0
USB_D_-
Wire Wire Line
	2000 1325 2075 1325
Text GLabel 2000 1325 0    50   Input ~ 0
USB_D_+
Wire Wire Line
	2975 1325 2875 1325
Text GLabel 2000 1525 0    50   Output ~ 0
D_+
Wire Wire Line
	2975 1525 2875 1525
Text GLabel 2975 1525 2    50   Output ~ 0
D_-
Wire Wire Line
	2000 1525 2075 1525
$EndSCHEMATC
