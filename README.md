# Isolated USB-UART converter

Simple USB-UART converter based on MCP2200 chip and using ADUM12XX digital
isolators to separate two endpoint devices.

It could be especially useful when talking to some external devices using mains
voltage. I wouldn't want to fry my :computer: by mistake :wink:.

Provided signals: `TX`, `RX`, `RTS`, `CTS`, `USBCFG`, `USBSSPD`

I needed to minimise PCB area so I cut it under the USB port. That's why
silkscreen floats ugly there.

Top:

![top](imgs/isolated-uart-top.png)

Bottom:

![bot](imgs/isolated-uart-bot.png)

